use company;
#For each employee, get the employee last name and the last name of their department manager.

select e1.last_name, e2.last_name
from employee e1, employee e2, department d
where e1.dno=d.dnumber and d.mgrssn = e2.ssn and e1.last_name != e2.last_name
order by e1.last_name asc;

select e1.last_name, e2.last_name
from employee e1 join department on e1.ssn = department.mgrssn
where e1.ssn in 
(select e2.ssn
from employee e2);

#Salaries of all employees have been incremented by $1000 plus 3% of their previous salaries.

Select * from employee;

Update employee set salary = salary * 1.03 + 1000;

#Compute the min, max and average salaries grouped by sex
Select sex, min(salary) from employee group by sex;
Select sex, max(salary) from employee group by sex;
Select sex, avg(salary) from employee group by sex;