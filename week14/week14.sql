#initial time:2.875
#after index:1.907
#after PK:0.391
use exercise;

select * from proteins;
load data local infile 'C:\\Users\\abdullah\\Desktop\\insert.txt' into table proteins fields terminated by '|';

select *
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on proteins(uniprot_id);
drop index uniprot_index on proteins;

alter table proteins add constraint pk_proteins primary key (uniprot_id);